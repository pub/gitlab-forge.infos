# Décommissionnement des outils de la PIC

Cette feuille de route a été présentée en Comité Technique Livraison continue le 01/07/2022 suite à une première communication fin 2021, puis communiquée en CODIR.

Elle a été aussi présentée au groupe de travail GTI en septembre 2022.

Son contenu est amendé sur les alternatives par le biais des discussions avec les utilisateurs pour mieux les orienter.

Tableau mis à jour : 21/11/2023

| Outil | Hébergement | Actions | Remplacé par |
| :---- | :---------: | :-----: | :----------- |
| TestLink | Legacy : Bdx[^legacy] | Outil décommissionné 17/11/2023 <br/> | [Portail SQUASH](https://snum.gitlab-pages.din.developpement-durable.gouv.fr/dam/squash-infos/) <br> Ariane : [#Squash-forge](https://ariane.din.developpement-durable.gouv.fr/channel/Squash-forge). |
| Mantis | Legacy : Bdx[^legacy] | Outil décommissionné 17/11/2023 | Gitlab Issues (avec pièces attachées et champs personnalisés) - interfacage avec [Portail SQUASH](https://snum.gitlab-pages.din.developpement-durable.gouv.fr/dam/squash-infos/) |
| Jenkins | Cloud : Paris | Outil décommissionné 17/11/2023 | [GitLab CI/CD]( https://gitlab-forge.din.developpement-durable.gouv.fr/) <br> Ariane : [#gitlab-forge](https://ariane.din.developpement-durable.gouv.fr/channel/gitlab-forge)|
| Nexus | Cloud : Paris | Outil décommissionné 28/08/2024 | GitLab registries |
| Git/Redmine | Paris | **Depuis Mai 2023 : Lecture seule** | GitLab<br/>et [Portail SPS pour les tickets](https://portail-support.din.developpement-durable.gouv.fr/) |
| Sonarqube | Cloud : Paris | Eco4 : depuis Mai 2023 | [Sonarqube cloud ECO4](https://sonarqube-forge.din.developpement-durable.gouv.fr/) <br>Ariane : [#Sonarqube-forge](https://tchap.gouv.fr/#/room/#Sonarqube-forge:agent.dev-durable.tchap.gouv.fr) |


---


[^legacy]: Les outils installés sur l'hébergement de Bordeaux (Legacy) ne pourront être maintenus au delà de la fermeture du CS de Bordeaux :grin:
