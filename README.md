# Page d'information sur gitlab-forge<!-- omit from toc -->

Cette page permet de vous informer des fonctionnalités actuellement en production ainsi que les procédures si vous avez besoin d'un espace.

N'hésitez pas à proposer un merge request si vous voyez des erreurs ou discuter de Gitlab sur le canal tchap [#gitlab-forge](https://www.tchap.gouv.fr/#/room/#gitlab-forge:agent.dev-durable.tchap.gouv.fr)

Gitlab-forge est mise à disposition des services du ministère. La version de GitLab installée est indiquée sur l'en-tête du canal.

Gitlab-forge est accessible à partir d'intranet ainsi qu'à partir d'internet en https (aucun accès en ssh pour le moment).

Pour l'**assistance à gitlab-forge** :

- *"Faire une demande d'assistance"* sur le [portail Architecture et Méthodes](https://portail.centre-serveur.din.developpement-durable.gouv.fr/projects/demande-architecture-et-methode)
- ou sur le canal tchap [#gitlab-forge](https://www.tchap.gouv.fr/#/room/#gitlab-forge:agent.dev-durable.tchap.gouv.fr), si vous voulez partager ou vous faire aider par DAM ou d'autres utilisateurs, pour des problèmes que d'autres peuvent rencontrer par exemple.

Pour de plus amples informations sur GitLab-forge, une [documentation](https://snum.gitlab-pages.din.developpement-durable.gouv.fr/dam/gitlab/gitlab-forge-doc/) décrivant cet éco-système est consultable pour les membres de GitLab-forge. Cette documentation est mise à jour avec les évolutions de cette plate-forme.

- [Accès](#accès)
  - [Agent du ministère](#agent-du-ministère)
    - [Accés aux groupes GitLab](#accés-aux-groupes-gitlab)
  - [Partenaires externes](#partenaires-externes)
    - [Création du compte par le partenaire externe](#création-du-compte-par-le-partenaire-externe)
  - [Accès public](#accès-public)
- [Exploitation](#exploitation)
  - [Mise en production](#mise-en-production)
  - [Mise en condition de sécurité](#mise-en-condition-de-sécurité)
  - [Sauvegardes](#sauvegardes)
    - [Impact de la sauvegarde](#impact-de-la-sauvegarde)
- [Fonctionnalités](#fonctionnalités)
  - [Dépôt git](#dépôt-git)
  - [Container Registry](#container-registry)
  - [Runner](#runner)
    - [Caractéristiques actuelles du runner mis à disposition](#caractéristiques-actuelles-du-runner-mis-à-disposition)
  - [GitLab Pages](#gitlab-pages)
  - [Dependency proxy](#dependency-proxy)

## Accès

### Agent du ministère

Tous les agents du pôle ministériel, ayant une adresse en @developpement-durable.gouv.fr ou @i-carre.net, peuvent accéder à Gitlab-forge (à l'aide de l'identifiant et mot de passe de la messagerie) et y créer des projets individuels sous leur profil.

 **Il est interdit de créer un projet public ou des GitLab Pages sous votre profil individuel GitLab.** Dans ce cas, l'URL est publiée avec votre nom de compte et n'est donc pas pérenne. Par ailleurs, Gitlab-forge n'est pas destiné à publier des projets personnels, ni des GitLab Pages individuels. Un script automatique est exécuté réguliérement pour enlever la visibilité publique sur les projets individuels, ainsi que les Gitlab Pages individuelles.

#### Accés aux groupes GitLab

Pour rejoindre un groupe ou projet GitLab existant, il faut demander au gestionnaire de groupe ou projet GitLab de vous ajouter dans celui-ci via le menu (*Group information | Project information - members*) **après** que vous vous soyez connecté une première fois à GitLab (sinon le gestionnaire ne verra pas votre compte utilisateur).

*Note*: répondre aux invitations par mail ne peut pas fonctionner car les comptes locaux GitLab sont désactivés.

S'il n'y a pas encore de groupe GitLab correspondant à votre service, afin de pouvoir travailler en équipe, vous pouvez en demander la création, sur le [portail Architecture et Méthodes](https://portail.centre-serveur.din.developpement-durable.gouv.fr/projects/demande-architecture-et-methode) - *"Faire une demande d'assistance"*, et indiquer obligatoirement :

- la motivation de cette création ;
- le nom du groupe correspondant au service du demandeur ;
- et le *owner* du groupe (l'administrateur)  qui aura la possibilité de créer des sous-groupes ou projets au sein de ce groupe ainsi que les permissions. **L'administrateur doit appartenir au service créé, et doit transmettre un accord écrit de la part du responsable hiérarchique de ce service**. Cet administrateur devient alors responsable du fonctionnement de son groupe.

*Note* : pour être administrateur d'un groupe racine (tel qu'une direction par ex.), la demande devra toutefois être validée par la hiérarchie du niveau approprié. Dans ce cas, le responsable GitLab de ce groupe sera *Maintainer* et sera donc automatiquement owner des sous-groupes qu'il créeras.

Les groupes et projets peuvent être internes ou privés :

- un groupe ou projet interne est lisible par tous les agents pouvant se connecter à Gitlab-forge ;
- un groupe ou projet privé ne sera accessible qu'aux membres de celui-ci.

A votre demande, nous pouvons également créer un sous-groupe du **groupe "[pub](https://gitlab-forge.din.developpement-durable.gouv.fr/pub)" dédié aux groupes publics**. La visibilité des sous-groupes/projets de "pub" pourra être interne, privée ou publique.

- un groupe/projet public est accessible à tout internet sans authentification.

### Partenaires externes

Les partenaires externes qui développent des produits ministériels peuvent accéder aux différents outils mis à disposition de la DNUM tels que Gitlab, Taiga, Sonarqube selon **2 conditions** qui sont l'**authentification** et l'**accréditation**

#### Création du compte par le partenaire externe

- **Etape 1** : **[Créer son compte sur le portail Cerbère](https://authentification.din.developpement-durable.gouv.fr/authSAML/moncompte/gestion/accueil.do)**.  
Le partenaire externe crée directement son compte professionnel en renseignant son numéro Siren.

- **Etape 2** : [**Demander l'accréditation**](https://portail.centre-serveur.din.developpement-durable.gouv.fr/projects/demande-architecture-et-methode/issues/new?template_id=105) aux outils en créant un Ticket DAM (portail SPS - Département Architecture et Méthodes).  
Dès que le compte cerbère est créé, le responsable "produit" du ministère fait une demande d'accréditation via le portail SPS, en renseignant dans l'**objet du ticket** : **Accréditation à l'outil "XX"**, l'adresse **mail du partenaire externe** et l'**application concernée** (GitLab ou Taïga)

- **Etape 3** : **S'authentifier sur l'application GitLab (ou Taïga)** : le partenaire externe s'authentifie  une **première fois** **(obligatoire !)**.

- **Etape 4** : **Inviter le partenaire externe à rejoindre votre projet** : En tant qu'agent du ministère vous l'invitez à rejoindre votre équipe sur votre projet depuis l'application concernée (Gitlab ou Taiga).  

### Accès public

Si vous devez publier/partage un groupe ou projet public sur internet sans authentification, vous devez en faire la demande sur le [portail Architecture et Méthodes](https://portail.centre-serveur.din.developpement-durable.gouv.fr/projects/demande-architecture-et-methode) en précisant :

- le nom du groupe correspondant au service du demandeur ;
- ainsi que le propriétaire du groupe (l'administrateur) qui aura la possibilité de créer des sous-groupes ou projets (public, interne ou privé) au sein de ce groupe.

Seul le groupe racine [/pub](https://gitlab-forge.din.developpement-durable.gouv.fr/pub) est autorisé, par défaut, à contenir des groupes/projets publics. Cela reste possible pour les autres groupes après validation des administrateurs GitLab et une demande sur le [portail Architecture et Méthodes](https://portail.centre-serveur.din.developpement-durable.gouv.fr/projects/demande-architecture-et-methode) en précisant les motivations de l'accès public.

## Exploitation

Pour toutes les mises en production (MEP) :
- un bandeau GitLab la signale,
- un message dans la lettre d'actualité MSP l'indique,
- un avertissement dans le canal tchap [#GitLab-forge](https://www.tchap.gouv.fr/#/room/#gitlab-forge:agent.dev-durable.tchap.gouv.fr) la rappelle,
- enfin le [tableau de bord du PSIN](http://psin.supervision.e2.rie.gouv.fr/supervision/psin/portail5.php) l'affiche.

### Mise en production

Le 3ème mardi de chaque est sanctuarisé pour les MEP standard (changement d'infra par ex.).  
**Ces mises à jours sont effectuées en heures non ouvrées (HNO) à partir de 19h30 pour un arrêt d'au moins 2h.**

### Mise en condition de sécurité

Ces mises à jours peuvente être effectuées n'importe quel jour de la semaine, avec une préférence pour les mardi et jeudi.  
**Ces mises à jours sont effectuées en HNO à partir de 19h30 pour un arrêt de 2h maximum.**


### Sauvegardes

Gitlab-forge est sauvegardé une fois par jour avec une rétention d'une semaine sur le BaaS (stockage objet fournis par DIS/GIC).  
Plus précisement, il y a une sauvegarde par jour ouvert et une sauvegarde le premier week-end du mois.

**La restauration consiste à restaurer le serveur complet !**

**Il n'est pas possible de restaurer à la carte** : un groupe ou projet supprimé par mégarde par exemple.

#### Impact de la sauvegarde

Afin d'éviter toutes écritures sur les dépôts git des projets GitLab, le service gitlay est stoppé durant la sauvegarde quotidienne qui est executée à 23h. **Le service GitLab est donc interrompu (erreur 500) entre 23h30 et 0h00.** (sauf le premier lundi du mois où c'est plus long)

## Fonctionnalités

### Dépôt git

Le dépôt est accessible en `https` uniquement. Les utilisateurs doivent créer, puis utiliser [un jeton (token)](https://gitlab-forge.din.developpement-durable.gouv.fr/-/profile/personal_access_tokens) qui sert de mot de passe pour le client git :

- login : *login Gitlab-forge*, souvent prenom.nom, indiqué en haut à droite lorsque vous êtes connecté ;
- mot de passe : *token* que vous aurez soigneusement noté (s'il est perdu vous pouvez en créer un autre et résiler l'ancien).

Le git clone sera une adresse en https. Vous saisissez alors les identifiants demandés. Afin de ne pas saisir ces identifiants à chaque commande distante de git, cette commande les garde en cache mémoire un certain temps pour votre dépôt :

```bash
git config credential.helper 'cache –timeout=43200'
```

Il existe d'autres méthodes de stockage, en fonction de vos besoins et de votre système. Cf. la [documentation de Git](https://git-scm.com/book/fr/v2/Utilitaires-Git-Stockage-des-identifiants).

> :warning: :warning: :warning: Même si cela est possible techniquement, **ne pas insérer les identifiants dans l'URL**, pour des raisons de sécurité. Voici un exemple de **mauvaise pratique** :
>
> ```bash
> git clone https://Prenom.Nom:LeToken@gitlab-forge.din.developpement-durable.gouv.fr/truc/machin.git/
> ```
> 
> Si vous transmettez une archive de votre dépôt Git à un partenaire, il aura accès à votre token ! (en clair dans le dossier .git/config).

### Container Registry

Le registry de Gitlab-forge est mis à disposition sur *intranet* et *internet*.

Pour activer la fonctionalité pour un projet: *Settings - General - Visibility, project features, permissions - Container registry*, ensuite l'URL est affichée dans le menu du projet : *Packages et Registries - Container Registry - CLI Commands*

Note: Pensez à activer le nettoyage des images de vos projets dans le menu du projet afin de préserver l'espace disque: Settings -> Clean up image tags -> Clean up image tags.

**Merci d'y faire le ménage réguliérement** afin de ne pas saturer inutilement le disque du serveur et de veiller à créer des images minimalistes, dans la mesure du possible, pour ne pas saturer rapidement le serveur.

### Runner

Si vous disposez déjà de votre propre runner local ou comptez disposer de votre propre runner, vous pouvez le [connecter à Gitlab-forge](https://docs.gitlab.com/runner/#runner-registration).

Deux runners partagés sont mis à disposition des utilisateurs.

#### Caractéristiques actuelles du runner mis à disposition

- Ce runner partagé se base sur un [Kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html). Il permet donc de lancer des builds depuis une image de votre choix, mais ne permet pas de faire du Docker-in-Docker ;
- Pour construire une image, vous pouvez néanmoins utiliser [kaniko](https://github.com/GoogleContainerTools/kaniko) ou [podman](https://podman.io/docs). Des exemples d'utilisation sont disponibles dans le composant [OCI builder](https://gitlab-forge.din.developpement-durable.gouv.fr/share/components/oci-builder).

### GitLab Pages

Les GitLab pages sont **opérationnelles et en https sur internet**.

L'accés des pages est déterminée au niveau de chaque projet et suivant sa nature (public, interne ou privé) du projet. Il devient alors possible de rendre les pages accessibles à tous ou seulement si on est identifié sur Gitlab-forge ou uniquement aux membres du projet.

Les Gitlab pages sont générées par un runner.
